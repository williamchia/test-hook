'use strict';

const https = require('https');

module.exports.hello = async (event) => {
  console.log("Starting function");
  console.log("event : ", event);
  const body = JSON.parse(event.body);

  if (body.object_kind == 'issue' &&
    body.object_attributes.action == 'open') {
      let message = "Thanks for creating a new issue."
      message = encodeURIComponent(message);
      const project_id = body.project.id;
      const issue_iid = body.object_attributes.iid;
      const path = "/api/v4/projects/" + project_id + "/issues/" + issue_iid + "/notes?body=" + message
      const ACCESS_TOKEN = process.env.ACCESS_TOKEN;
      const headers = {
        'PRIVATE-TOKEN': ACCESS_TOKEN
      }

      return new Promise((resolve, reject) => {
          const options = {
            hostname: 'gitlab.com',
            port: 443,
            path: path,
            method: 'POST',
            headers: headers,
          }

          const req = https.request(options, (res) => {
            let body = '';
            res.on('data', (chunk) => (body += chunk.toString()));
            res.on('error', (err) => (reject(err.message)));
            res.on('end', () => {
              resolve({
                "isBase64Encoded": false,
                "statusCode": res.statusCode,
                "headers": {
                  'Access-Control-Allow-Origin': '*',
                  'Access-Control-Allow-Credentials': true,
                },
                "body": JSON.stringify({
                  "message": "Fuction executed",
                  "reqBody": JSON.parse(event.body),
                  "resBody": JSON.parse(body)
                })
              });
            });
          });

          req.on('error', (err) => (reject(err.message)));
          req.write('');
          req.end();
      });
    } else {
      return 'Not new issue event'
    }
};
